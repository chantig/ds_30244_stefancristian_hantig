#README

#Introduction
	Proiectul presupune o platforma de gestionare a zborurilor. Aceasta aplicatie poate sa fie folosita de catre 
	Regular user, cat side un admin user. Adminul are rolul de a face CRUD pe obiectele "Flight".
#Tehnologii folosite
Pentru partea de server am folosit tom versiunea 8. Pentru a rezolva cereririle s-a folosit JAVA servlets. 
Dependentele de "JAR"-uri au fost rezolvate folosind maven. Acestea pot fi gasite in fisierul pom.xml.
Partea de front-end a fost implementata folosit JSP. Aceasta a permis vizualizarea structurilor repetitive.
##Database model
![DB diagram](images/database_model.png)

##Deplyment
![Deployment diagram](images/deplymenta12.png)

##Conceptual architecture
![Architecture diagram](images/architecture.png)

