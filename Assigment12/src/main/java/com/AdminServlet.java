package com;

import persistence.dao.FlightDAO;
import persistence.model.Flight;
import util.db.HibernateUtil;
import util.validator.FlightValidator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "AdminServlet",urlPatterns = {"/adminInsert","/adminUpdate","/adminDelete"})
public class AdminServlet extends HttpServlet {
    private final static FlightDAO FLIGHT_DAO = new FlightDAO(HibernateUtil.getSessionFactory());
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String url = request.getRequestURI().toString();
        RequestDispatcher requestDispatcher = null;
        switch (url){
            case "/adminInsert":
                doInsert(request,response);
                break;
            case "/adminUpdate":
                doUpdate(request,response);
                break;
            case "/adminDelete":
                doDelete(request.getParameter("id"));
                break;
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    private void doInsert(HttpServletRequest request,HttpServletResponse response){
        String type = request.getParameter("type");
        String dcity = request.getParameter("dcity");
        String acity = request.getParameter("acity");
        String dhour = request.getParameter("dhour");
        String ahour = request.getParameter("ahour");
        Flight flight = new Flight(type,dcity,acity,dhour,ahour);
        Integer id = -1;
        if (FlightValidator.validate(flight)){
            id = FLIGHT_DAO.insert(flight);
        }
        try {
            response.getWriter().write(id.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void doDelete(String id){
        Flight flight = new Flight();
        try {
            flight.setId(Integer.parseInt(id));
            FLIGHT_DAO.delete(flight);
        }catch (Exception e){

        }
    }
    private void doUpdate(HttpServletRequest request,HttpServletResponse response){
        int id = Integer.parseInt(request.getParameter("id"));
        String type = request.getParameter("type");
        String dcity = request.getParameter("dcity");
        String acity = request.getParameter("acity");
        String dhour = request.getParameter("dhour");
        String ahour = request.getParameter("ahour");
        Flight flight = new Flight(type,dcity,acity,dhour,ahour);
        Integer valid = -1;
        if (FlightValidator.validate(flight)) {
            flight.setId(id);
            FLIGHT_DAO.update(flight);
            valid = 0;
        }
        try {
            response.getWriter().write(valid.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
