package com;

import persistence.dao.CityDAO;
import persistence.dao.FlightDAO;
import persistence.dao.UserDAO;
import persistence.model.City;
import persistence.model.Flight;
import persistence.model.User;
import util.conveter.UserTypeConverter;
import util.db.HibernateUtil;
import util.type.UserType;
import util.validator.PasswordValidator;
import util.validator.UsernameValidator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "Login", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {
    private final static UserDAO userDAO = new UserDAO(HibernateUtil.getSessionFactory());
    private final static FlightDAO flightDAO = new FlightDAO(HibernateUtil.getSessionFactory());
    private final static CityDAO CITY_DAO = new CityDAO(HibernateUtil.getSessionFactory());
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        boolean valid = PasswordValidator.validate(password) && UsernameValidator.validate(username);
        User user = userDAO.findByUsernamePassword(username,password);
        RequestDispatcher requestDispatcher = null;
        HttpSession session = request.getSession(true);
        if (user == null || !valid){
            request.setAttribute("success","Login Unsuccessful");
            request.setAttribute("color","red");
            requestDispatcher = request.getRequestDispatcher("login.jsp");

        }else {
            List<Flight> flights = flightDAO.findAll();
            switch (user.getType()){
                case ADMIN:
                    session.setAttribute("user",UserTypeConverter.convert(user.getType()));
                    request.setAttribute("user",UserTypeConverter.convert(user.getType()));
                    request.setAttribute("flights",flights);
                                        requestDispatcher = request.getRequestDispatcher("admin.jsp");
                    break;
                case REGULAR:
                    session.setAttribute("user",UserTypeConverter.convert(user.getType()));
                    request.setAttribute("user",UserTypeConverter.convert(user.getType()));
                    request.setAttribute("flights",flights);
                    request.setAttribute("arrivals",getFlightsArrivalCities(flights));
                    request.setAttribute("departures",getFlightsDepartCities(flights));
                    requestDispatcher = request.getRequestDispatcher("regular.jsp");
                    break;
            }
        }
        requestDispatcher.forward(request,response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
    private List<City> getFlightsArrivalCities(List<Flight> flights){
        List<City> cities = new ArrayList<>();
        for (Flight flight:flights){
            cities.add(CITY_DAO.findByName(flight.getArrivName()));
        }
        return cities;
    }
    private List<City> getFlightsDepartCities(List<Flight> flights){
        List<City> cities = new ArrayList<>();
        for (Flight flight:flights){
            cities.add(CITY_DAO.findByName(flight.getDepartName()));
        }
        return cities;
    }

}
