package com;

import persistence.dao.UserDAO;
import persistence.model.User;
import util.db.HibernateUtil;
import util.type.UserType;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Reserve", urlPatterns = {"/register"})
public class RegisterServlet extends HttpServlet {
    private final static UserDAO userDAO = new UserDAO(HibernateUtil.getSessionFactory());
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        User user = userDAO.findByUsername(username);
        RequestDispatcher requestDispatcher = null;

        if (user == null){
            User user1 = new User(name,username,password, UserType.REGULAR);
            userDAO.insert(user1);
            request.setAttribute("color","green");

            request.setAttribute("success","Registration Successful");
            requestDispatcher = request.getRequestDispatcher("login.jsp");
        }else{
            request.setAttribute("success","Registration Unsuccessful");
            requestDispatcher = request.getRequestDispatcher("register.jsp");
        }
        requestDispatcher.forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
