package persistence.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import persistence.model.City;
import util.db.HibernateUtil;

import java.util.List;

public class CityDAO extends StandardDAO<City>{

    public CityDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
    public List<City> findAll(){
        return abtractQuery("from City");
    }
    public City findById(int id){
        List<City> cities = abtractQuery("from City where id=" + id);
        if (cities.size() == 1)
        {
            return cities.get(0);
        }
        return null;
    }
    public City findByName(String name){
        List<City> cities = abtractQuery("from City where name=\'" + name + "\'");
        if (cities.size() == 1)
        {
            return cities.get(0);
        }
        return null;
    }

}

