package persistence.dao;

import org.hibernate.SessionFactory;
import persistence.model.Flight;

import java.util.List;

public class FlightDAO extends StandardDAO<Flight>{


    public FlightDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
    public List<Flight> findAll(){
         return abtractQuery("from Flight");
    }
    public Flight findById(int id){
        List<Flight> flights = abtractQuery("from Flight where id=" + id);
        if (flights.size() == 1){
            return flights.get(0);
        }
        return null;
    }

}
