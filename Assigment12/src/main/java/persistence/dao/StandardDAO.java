package persistence.dao;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.lang.reflect.ParameterizedType;
import java.util.List;

public abstract class StandardDAO<T> {
    private SessionFactory sessionFactory;
    private final Class<T> type;

    @SuppressWarnings("unchecked")
    StandardDAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public Integer insert(T t){
        boolean inserted = false;
        Session session = this.sessionFactory.openSession();
        Transaction transaction = null;
        Integer id = -1;
        try{
            transaction = session.beginTransaction();
            id = (Integer)session.save(t);
            transaction.commit();
            inserted = true;
        }catch (HibernateException e){
            if (transaction!=null){
                transaction.rollback();
            }
        }
        return id;
    }
    public boolean delete(T t){
        boolean deleted = false;

        Transaction transaction = null;
        try(Session session = this.sessionFactory.openSession()){
            transaction = session.beginTransaction();
            session.delete(t);
            transaction.commit();
            deleted = true;
        }catch (HibernateException e){
            if (transaction != null){
                transaction.rollback();
            }
        }
        return deleted;
    }
    public boolean update(T t){
        boolean updated = false;
        Transaction transaction = null;
        try (Session session = this.sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.update(t);
            transaction.commit();
            updated = true;
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return updated;
    }
    List<T> abtractQuery(String query){
        List<T> ts = null;
        Transaction transaction = null;
        try (Session session = this.sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            ts = session.createQuery(query, type).list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return ts;
    }

}
