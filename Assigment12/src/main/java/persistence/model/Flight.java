package persistence.model;

import com.sun.istack.internal.NotNull;

import javax.persistence.*;

@Entity
@Table(name = "flight")
public class Flight {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    @Column(name = "id")
    private int id;
    @Column(name = "airplane_type")
    private String type;
    @Column(name = "depart_city")
    private String departName;
    @Column(name = "arriv_city")
    private String arrivName;
    @Column(name = "depart_hour")
    private String departHour;
    @Column(name = "arriv_hour")
    private String arrivHour;

    public Flight() {
    }

    public Flight(String type, String departName, String arrivName, String departHour, String arrivHour) {
        this.type = type;
        this.departName = departName;
        this.arrivName = arrivName;
        this.departHour = departHour;
        this.arrivHour = arrivHour;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDepartName() {
        return departName;
    }

    public void setDepartName(String departName) {
        this.departName = departName;
    }

    public String getArrivName() {
        return arrivName;
    }

    public void setArrivName(String arrivName) {
        this.arrivName = arrivName;
    }

    public String getDepartHour() {
        return departHour;
    }

    public void setDepartHour(String departHour) {
        this.departHour = departHour;
    }

    public String getArrivHour() {
        return arrivHour;
    }

    public void setArrivHour(String arrivHour) {
        this.arrivHour = arrivHour;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", departName='" + departName + '\'' +
                ", arrivName='" + arrivName + '\'' +
                ", departHour='" + departHour + '\'' +
                ", arrivHour='" + arrivHour + '\'' +
                '}';
    }
}
