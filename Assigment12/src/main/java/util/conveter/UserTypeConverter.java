package util.conveter;

import util.type.UserType;

public class UserTypeConverter {
    public static UserType convert(String stringType){
        switch (stringType){
            case "ADMIN":
                return UserType.ADMIN;
            case "REGULAR":
                return UserType.REGULAR;
        }
        return UserType.NONE;
    }
    public static String convert(UserType userType){
        switch (userType){
            case REGULAR:
                return "REGULAR";
            case ADMIN:
                return "ADMIN";
        }
        return "";
    }
}
