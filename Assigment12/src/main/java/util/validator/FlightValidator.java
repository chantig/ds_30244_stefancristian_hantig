package util.validator;

import persistence.model.Flight;

public class FlightValidator {
    public static boolean validate(Flight flight){
        if(!UsernameValidator.validate(flight.getType()))
            return false;
        if(!UsernameValidator.validate(flight.getArrivName()))
            return false;
        if(!UsernameValidator.validate(flight.getDepartName()))
            return false;
        if(!TimeValidator.validate(flight.getArrivHour()))
            return false;
        if(!TimeValidator.validate(flight.getDepartHour()))
            return false;
        if (flight.getDepartHour().compareTo(flight.getArrivHour()) > 0)
            return false;
        return true;
    }
}
