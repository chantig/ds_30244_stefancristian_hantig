package util.validator;

public class PasswordValidator {

    private final static String PASSWORD_FORMAT = "[a-zA-Z0-9]{6}[a-zA-Z0-9]*";
    public static boolean validate(String password){
        return StandardValidator.validate(password,PASSWORD_FORMAT);
    }
}
