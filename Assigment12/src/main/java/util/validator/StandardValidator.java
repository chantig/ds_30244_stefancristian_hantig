package util.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StandardValidator {
    private static Pattern pattern;
    public static boolean validate(String standard, String fromat) {
        pattern = Pattern.compile(fromat);
        Matcher matcher = pattern.matcher(standard);
        return matcher.matches();
    }
}
