package util.validator;

public class TimeValidator {
    private final static String DATE_FORMAT = "^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\\.[0-9]{3}$";
    public static boolean validate(String time){
        return StandardValidator.validate(time,DATE_FORMAT);
    }
}
