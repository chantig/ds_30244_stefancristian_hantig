package util.validator;

import persistence.model.User;

public class UserValidator {
    public boolean validate(User user){
        if(!UsernameValidator.validate(user.getUsername()))
            return false;
        if(!UsernameValidator.validate(user.getName()))
            return false;
        if (!UsernameValidator.validate(user.getPassword()))
            return false;
        return true;
    }
}
