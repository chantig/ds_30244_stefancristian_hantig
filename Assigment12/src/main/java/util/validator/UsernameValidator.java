package util.validator;

public class UsernameValidator {
    private final static String USERNAME_FORMAT = "[a-zA-Z][a-zA-Z0-9]{4}[a-zA-Z0-9]*";
    public static boolean validate(String username){
        return StandardValidator.validate(username,USERNAME_FORMAT);
    }
}
