<%@ page import="java.util.List" %>
<%@ page import="persistence.model.Flight" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/myStyle/loregstyle.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {

        $(document).on('click', '.del-button',function () {
            var ids = $(this).attr('id');
            var id = ids.substring(5,ids.length);
            $.ajax({
                type:"POST",
                url:"/adminDelete",
                data: "id="+id,
                success:function () {
                    $("#row"+ id).empty();
                    $("#row"+ id).remove();
                }
            });
        });
            $(document).on('click', '.up-button',function () {
                var ids = $(this).attr('id');
                var id = ids.substring(4,ids.length);
                var type = $("#type"+id).text();
                var dcity = $("#dcity"+id).text();
                var acity = $("#acity"+id).text();
                var dhour = $("#dhour"+id).text();
                var ahour = $("#ahour"+id).text();
                $.ajax({
                    type:"POST",
                    url:"/adminUpdate",
                    data: "id="+id+"&type="+type + "&dcity="+dcity+"&acity="+acity+"&dhour="+dhour+"&ahour="+ahour,
                    success:function (data) {
                        if (data != "-1")
                            alert("Update successful");
                        else
                            alert("Update unsuccessful");

                    }
                });
            });
        $("#insertFlg").click(function () {
            var type = $("#type").val();
            var dcity = $("#dcity").val();
            var acity = $("#acity").val();
            var dhour = $("#dhour").val();
            var ahour = $("#ahour").val();
            $.ajax({
                type:"POST",
                url:"/adminInsert",
                data: "type="+type + "&dcity="+dcity+"&acity="+acity+"&dhour="+dhour+"&ahour="+ahour,
                success:function (data) {
                    if (data != "-1"){
                        $("#flightTable").append($("<tr id=\"row" + data + "\">" +
                            "<td id=\"fid"+ data +"\">" + data +"</td>" +
                            "<td id=\"type"+ data +"\">" + type +"</td>" +
                            "<td id=\"dcity"+data+"\">" + dcity +"</td>" +
                            "<td id=\"acity"+data+"\">" + acity +"</td>" +
                            "<td id=\"dhour"+data+"\">" + dhour +"</td>" +
                            "<td id=\"ahour"+ acity +"\">" + ahour +"</td>" +
                            "<td> <button id=\"delid"+data+"\" class=\"del-button btn btn-primary\">Delete</button>" +
                            "<button id=\"upid"+data+"\" class=\"up-button btn btn-primary\">Update</button> </td>" +
                            "</tr>"));
                    }
                }
            })
        })
        });
    </script>
</head>
<body id="LoginForm">
<%
    String user = (String)request.getAttribute("user");
    if (user != null && user.equals("ADMIN")){
%>
<nav class="navbar navbar-expand-md justify-content-md-end" style="background-color: #ff9900">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="/login.jsp" style="color: white">Logout</a>
        </li>
    </ul>
</nav>
<div class="container">

    <div class="fl-table">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Airplane Type</th>
                    <th>Depart City</th>
                    <th>Arrival City</th>
                    <th>Depart Hour</th>
                    <th>Arrival Hour</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody id="flightTable">
            <tr id="row0">
                <td></td>
                <td><input id="type" type="text" class="form-control" placeholder="Airplane Type" required name="aType"></td>
                <td><input id="dcity" type="text" class="form-control" placeholder="Depart City" required name="dCity"></td>
                <td><input id="acity" type="text" class="form-control" placeholder="Arrival City" required name="aCity"></td>
                <td><input id="dhour" type="text" class="form-control" placeholder="Depart Hour" required name="dHour"></td>
                <td><input id="ahour" type="text" class="form-control" placeholder="Arrival Hour" required name="aHour"></td>
                <td>
                    <button id="insertFlg" class="btn btn-primary">Insert</button> </td>
            </tr>
                <%
                    List<Flight> flights = (List<Flight>) request.getAttribute("flights");
                    for (Flight flight:flights){
                %>


                    <tr id="row<%=flight.getId()%>">
                        <td id="fid<%=flight.getId()%>"><%=flight.getId()%></td>
                        <td id="type<%=flight.getId()%>" contenteditable="true"><%=flight.getType()%></td>
                        <td id="dcity<%=flight.getId()%>" contenteditable="true"><%=flight.getDepartName()%></td>
                        <td id="acity<%=flight.getId()%>" contenteditable="true"><%=flight.getArrivName()%></td>
                        <td id="dhour<%=flight.getId()%>" contenteditable="true"><%=flight.getDepartHour()%></td>
                        <td id="ahour<%=flight.getId()%>" contenteditable="true"><%=flight.getArrivHour()%></td>
                        <td>
                            <button id="delid<%=flight.getId()%>" class="del-button btn btn-primary">Delete</button>
                            <button id="upid<%=flight.getId()%>" class="up-button btn btn-primary">Update</button> </td>
                    </tr>
                <%
                    }
                %>
            </tbody>

        </table>
    </div>
</div>
<%}else{%>
<nav class="navbar navbar-expand-md justify-content-md-end" style="background-color: #ff9900">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="/login.jsp" style="color: white">Login</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/login.jsp" style="color: white">Register</a>
        </li>
    </ul>
</nav>
<h3>YOU CAN'T DO THAT</h3>
<%}%>
</body>
</html>
