<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Register</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/myStyle/loregstyle.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script>
    function verifyPasswords() {
        var password1 = $("#inputRegPassword").val();
        var password2 = $("#inputRegConfPassword").val();
        var match = true;
        if (password1 != password2){
            match = false;
            $("#inputRegPassword").css("border-color", "red");
            $("#inputRegConfPassword").css("border-color", "red");

        }else{
            $("#inputRegPassword").css("border-color", "green");
            $("#inputRegConfPassword").css("border-color", "green");

        }
        return match;
    }
</script>
</head>
<body id="LoginForm">
<nav class="navbar navbar-expand-md justify-content-md-end" style="background-color: #ff9900">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="/login.jsp" style="color: white">Login</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/register.jsp" style="color: white">Register</a>
        </li>
    </ul>
</nav>
<div class="container">
    <div class="login-form">
        <div class="main-div">
            <form id="Login" method="POST" action="/register" onsubmit="return verifyPasswords()">
                <div class="form-group">
                <input type="text" class="form-control" id="inputUsername" placeholder="Username" required name="username">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="inputName" placeholder="Name" required name="name">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" id="inputRegPassword" placeholder="Password" required name="password">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" id="inputRegConfPassword" placeholder="Confirm Password" required>
                </div>
                <button type="submit" class="btn btn-primary">Register</button>
                <%
                    String success = (String)request.getAttribute("success");
                    if (success != null){
                %>
                <p style="color: red"> <%= success%></p>

                <%
                    }
                %>
            </form>
        </div>
    </div>

</div>
</body>
</html>
