<%@ page import="java.util.List" %>
<%@ page import="persistence.model.Flight" %>
<%@ page import="persistence.model.City" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Regular</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/myStyle/loregstyle.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        function calcArrivaTime(arrival,departlat,departlong,arrivallat,arrivallong) {
            var arrivalTimestamp = Date.parse(arrival);
            var depOffset = 0;
            var arrivOffset = 0;
            $.ajax({
                async:false,
                url:"https://maps.googleapis.com/maps/api/timezone/json?location="+departlat+","+departlong+"&timestamp="+
                    (Math.round((new Date().getTime())/1000)).toString() + "&key=AIzaSyAowapvRKfbsiz_sonWmR1pCd-aBceUADI",
            }).done(function(response){
                    depOffset = response.rawOffset;
                debugger;

            });

            $.ajax({
                async:false,
                url:"https://maps.googleapis.com/maps/api/timezone/json?location="+arrivallat+","+arrivallong+"&timestamp="+
                    (Math.round((new Date().getTime())/1000)).toString() + "&key=AIzaSyAowapvRKfbsiz_sonWmR1pCd-aBceUADI",
            }).done(function(response){
                    arrivOffset = response.rawOffset;
                    debugger;
            });
            var offfset = arrivOffset - depOffset;
            arrivalTimestamp = arrivalTimestamp + offfset * 1000;
            var arTime = new Date(arrivalTimestamp);
            alert(arTime.toString().substring(0,24));
        }
    </script>
</head>
<body id="LoginForm">
<%
    String user = (String)request.getAttribute("user");
    if (user != null && user.equals("REGULAR")){
%>
<nav class="navbar navbar-expand-md justify-content-md-end" style="background-color: #ff9900">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="/login.jsp" style="color: white">Logout</a>
        </li>
    </ul>
</nav>

<div class="container">

    <div class="fl-table">
        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>Airplane Type</th>
                <th>Depart City</th>
                <th>Arrival City</th>
                <th>Depart Hour</th>
                <th>Arrival Hour</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody id="flightTable">
            <%
                List<Flight> flights = (List<Flight>) request.getAttribute("flights");
                List<City> arrivCities = (List<City>) request.getAttribute("arrivals");
                List<City> departCities = (List<City>) request.getAttribute("departures");
                int i = 0;
                City arrivCity = null;
                City departCity = null;
                for (Flight flight:flights){
                    i = flights.indexOf(flight);
                    arrivCity = arrivCities.get(i);
                    departCity = departCities.get(i);
            %>


            <tr id="row<%=flight.getId()%>>">
                <td><%=flight.getType()%></td>
                <td><%=flight.getDepartName()%></td>
                <td><%=flight.getArrivName()%></td>
                <td><%=flight.getDepartHour()%></td>
                <td><%=flight.getArrivHour()%></td>
                <td>
                    <button class="btn btn-primary" onclick="calcArrivaTime('<%=flight.getArrivHour()%>',<%=departCity.getLatitude()%>,<%=departCity.getLongitude()%>,<%=arrivCity.getLatitude()%>,<%=arrivCity.getLongitude()%>)">
                        LocalHour</button> </td>
            </tr>
            <%
                }
            %>
            </tbody>

        </table>
    </div>
</div>
<%}else{%>
<nav class="navbar navbar-expand-md justify-content-md-end" style="background-color: #ff9900">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="/login.jsp" style="color: white">Login</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/login.jsp" style="color: white">Register</a>
        </li>
    </ul>
</nav>
    <h3>YOU CAN'T DO THAT</h3>
<%}%>
</body>
</html>
