package services;

import persistence.models.Nodes;
import persistence.models.Packs;
import persistence.models.Users;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService
public interface IClientUserService {
    @WebMethod
    public List<Packs> getAllPacks();
    @WebMethod
    public Nodes getState(Packs pack);
    @WebMethod
    public Users getUser(String username);
}
