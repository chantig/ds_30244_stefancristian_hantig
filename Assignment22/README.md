##Assignment22
#Introducere
Pentru a implementa acest proiect a fost nevoie sa folosesc JAVA rmi. Acest proiect presupune folosirea
unor obiecte expuse de un server. 
#Build and Execution
Pentru o buna executare a proiectul, acesta trebuie importat ca un proiect maven. Acesta se poate face in oricare IDE de dezvoltare.
Implementarea consta din trei module, fiecare avand un fisier pom. Inainte de a rula applicatia, trebuie setata variabila de mediu CLASSPATH.
Aceasta trebuie setata la directorul in care apar fisierele .class a proiectului(In cazul InteliiJ, acestea apar in target/classes a fiecarui modul).
Dupa ce a fost setat variabila de mediu, se va rula intai  serverul, si dupa clientul.

#Deployment Diagram
![Deployment diagram](images/deployment.png)
#Architecure Diagram
![Deployment diagram](images/arch.png)