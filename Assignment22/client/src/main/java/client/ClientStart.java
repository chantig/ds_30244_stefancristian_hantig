package client;

import client.controllers.CarController;
import client.views.ICarView;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import servicesIntefaces.ISellPrice;
import servicesIntefaces.ITaxPrice;

import java.io.FileInputStream;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ClientStart extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        try{
            Registry registry = LocateRegistry.getRegistry(1099);
            ISellPrice iSellPrice = (ISellPrice)registry.lookup("ISellService");
            ITaxPrice iTaxPrice = (ITaxPrice)registry.lookup("ITaxService");
            FXMLLoader fxmlLoader = new FXMLLoader();
            String fxmlDocPath = ".\\client\\src\\resources\\carInfo.fxml";
            FileInputStream fxmlStream = new FileInputStream(fxmlDocPath);
            AnchorPane root = fxmlLoader.load(fxmlStream);
            ICarView iCarView= fxmlLoader.getController();
            iCarView.setCarController(new CarController(iTaxPrice,iSellPrice));
            Scene scene = new Scene(root);
            primaryStage.setResizable(false);
            primaryStage.setScene(scene);
            primaryStage.setTitle("CarInfo");
            primaryStage.show();

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
