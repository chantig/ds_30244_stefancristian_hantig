package client.controllers;

import client.views.ICarView;
import entities.Car;
import servicesIntefaces.ISellPrice;
import servicesIntefaces.ITaxPrice;

import java.rmi.RemoteException;

public class CarController implements ICarController{
    private final static String ERROR_MSG = "YEAR(Numbre), CAPACITY(Number) and PRICE(Number) should be positive";
    private ITaxPrice iTaxPrice;
    private ISellPrice iSellPrice;
    private ICarView iCarView;
    public CarController(ITaxPrice iTaxPrice, ISellPrice iSellPrice) {
        this.iTaxPrice = iTaxPrice;
        this.iSellPrice = iSellPrice;
    }

    public void setCarView(ICarView iCarView) {
        this.iCarView = iCarView;
    }

    @Override
    public void computeTax() throws RemoteException {
        boolean capacityFormat = iCarView.getCapacity().matches("^[1-9][0-9]*$");
        boolean yearFormat = iCarView.getCapacity().matches("^[1-9][0-9]*$") && capacityFormat;
        boolean priceFormat = iCarView.getCapacity().matches("^[1-9][0-9]*\\.?[0-9]*$") && yearFormat;
            if (priceFormat){
                int year = Integer.parseInt(iCarView.getYear());
                int capacity = Integer.parseInt(iCarView.getCapacity());
                Car car = new Car(year,capacity);
                iCarView.addOperation("tax(car[year="+car.getYear()+", capacity="+ car.getEngineCapacity()+"])=" +
                        iTaxPrice.computeTaxPrice(car));
            }else{
                iCarView.showError(ERROR_MSG);
            }
    }

    @Override
    public void computePrice() throws RemoteException {
        boolean capacityFormat = iCarView.getCapacity().matches("^[1-9][0-9]*$");
        boolean yearFormat = iCarView.getCapacity().matches("^[1-9][0-9]*$") && capacityFormat;
        boolean priceFormat = iCarView.getCapacity().matches("^[1-9][0-9]*\\.?[0-9]*$") && yearFormat;
        if (priceFormat){
            int year = Integer.parseInt(iCarView.getYear());
            int capacity = Integer.parseInt(iCarView.getCapacity());
            double price = Double.parseDouble(iCarView.getPrice());
            Car car = new Car(year,capacity,price);
            iCarView.addOperation("tax(car[year="+car.getYear()+", capacity="+ car.getEngineCapacity()+",price="+
                    car.getPrice()+"])=" + iSellPrice.computeSellPrice(car));
        }else{
        iCarView.showError(ERROR_MSG);
    }
    }

}
