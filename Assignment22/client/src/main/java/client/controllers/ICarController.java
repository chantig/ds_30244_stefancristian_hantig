package client.controllers;

import client.views.ICarView;
import java.rmi.RemoteException;

public interface ICarController {
    void computeTax() throws RemoteException;
    void computePrice() throws RemoteException;
    void setCarView(ICarView carView);
}
