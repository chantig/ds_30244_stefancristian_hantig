package client.views;

import client.controllers.ICarController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;

import java.net.URL;
import java.rmi.RemoteException;
import java.util.ResourceBundle;

public class CarView implements ICarView, Initializable {
    @FXML
    private TextField yearText;
    @FXML
    private TextField capacityText;
    @FXML
    private TextField priceText;
    @FXML
    private ListView<String> operationsList;
    private ObservableList<String> operations;
    private ICarController iCarController;

    public void setCarController(ICarController iCarController) {
        this.iCarController = iCarController;
        this.iCarController.setCarView(this);
    }

    @Override
    public void showError(String msg) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText("Format invalid");
        alert.setContentText(msg);
        alert.showAndWait();
    }

    @Override
    public String getYear() {
        return yearText.getText();
    }

    @Override
    public String getCapacity() {
        return capacityText.getText();
    }

    @Override
    public String getPrice() {
        return priceText.getText();
    }

    @Override
    public void addOperation(String operation) {
        operations.add(0,operation);
    }

    public void computeSell() {
        try {
            iCarController.computePrice();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void computeTax() {
        try {
            iCarController.computeTax();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        operations = FXCollections.observableArrayList();
        operationsList.setItems(operations);
    }
}
