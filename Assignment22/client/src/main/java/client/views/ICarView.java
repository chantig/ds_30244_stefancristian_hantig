package client.views;

import client.controllers.ICarController;

public interface ICarView {
    void showError(String msg);
    String getYear();
    String getCapacity();
    String getPrice();
    void setCarController(ICarController carController);
    void addOperation(String operation);

}
