package entities;
import java.io.Serializable;

public class Car implements Serializable {

    private static final long serialVersionUID = 1L;
    private int year;
    private int engineCapacity;
    private double price;

    public Car(int year, int engineCapacity) {
        this.year = year;
        this.engineCapacity = engineCapacity;
    }

    public Car(int year, int engineCapacity, double price) {
        this.year = year;
        this.engineCapacity = engineCapacity;
        this.price = price;
    }

    public int getYear() {
        return year;
    }


    public int getEngineCapacity() {
        return engineCapacity;
    }


    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Car [year=" + year + ", engineCapacity=" + engineCapacity + ", price="+price+"]";
    }

}
