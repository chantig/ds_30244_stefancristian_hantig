package servicesIntefaces;

import entities.Car;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ISellPrice extends Remote {
    double computeSellPrice(Car c) throws RemoteException;
}
