package servicesIntefaces;

import entities.Car;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ITaxPrice extends Remote {
    double computeTaxPrice(Car c) throws RemoteException;
}
