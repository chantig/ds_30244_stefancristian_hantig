package server;

import services.SellPriceService;
import services.TaxPriceService;
import servicesIntefaces.ISellPrice;
import servicesIntefaces.ITaxPrice;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class ServerStart {
    public static void main(String[] args) {
        try{
            LocateRegistry.createRegistry(1099);
            ISellPrice sellPriceService = new SellPriceService();
            ITaxPrice taxPriceService = new TaxPriceService();
            ISellPrice sellStub = (ISellPrice)UnicastRemoteObject.exportObject(sellPriceService,1099);
            ITaxPrice taxStub = (ITaxPrice)UnicastRemoteObject.exportObject(taxPriceService,1099);
            Registry registry = LocateRegistry.getRegistry();
            registry.rebind("ISellService",sellStub);
            registry.rebind("ITaxService",taxStub);
            System.out.println("Server is starting....");
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
