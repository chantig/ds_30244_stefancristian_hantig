package services;

import entities.Car;
import servicesIntefaces.ISellPrice;

import java.rmi.RemoteException;

public class SellPriceService implements ISellPrice {
    @Override
    public double computeSellPrice(Car c) throws RemoteException {
        double price;
        if (c.getPrice() <= 0) {
            throw new IllegalArgumentException("price must be positive.");
        }
        if (2018 - c.getYear() > 6){
            price = 0.0;
        }else{
            price = c.getPrice() - (c.getPrice() / 7.0)*(2018 - c.getYear());
        }
        return price;
    }
}
