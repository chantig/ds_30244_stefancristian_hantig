import com.rabbitmq.client.*;
import consumers.DVDConsumer;
import services.FileService;
import services.MailService;
import services.RegisterService;

public class ConsumerStart {
    private final static String QUEUE_NAME = "DVD";

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        MailService mailService = new MailService("cristianhantig96","Speolog12#");
        Consumer consumer = new DVDConsumer(channel,mailService);
        RegisterService.createReg();
        System.out.println("Waiting for mesages");
        channel.basicConsume(QUEUE_NAME, true, consumer);
    }
}
