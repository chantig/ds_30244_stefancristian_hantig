package consumers;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import services.MailService;
import services.RegisterService;
import utils.converters.ActionTypeConverter;

import java.io.IOException;
import java.util.List;

public class DVDConsumer extends DefaultConsumer {
    private MailService mailService;
    public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
        String message = new String(body, "UTF-8");
        System.out.println(message);
        String[] tokens = message.split("#");
        switch (ActionTypeConverter.convertFrom(tokens[0])){
            case UPDATE:
                onUpdate(tokens[2],tokens[1]);
                break;
            case REGISTER:
                RegisterService.addEmail(tokens[1]);
                break;
            case INSERT:
                onInsert(tokens[1]);
                break;
        }
    }

    public DVDConsumer(Channel channel, MailService mailService) {
        super(channel);
        this.mailService = mailService;
    }
    private void onInsert(String newDVD){
        doJob(newDVD, "NEW DVD");
    }
    private void onUpdate(String info, String subject){
        doJob(info, subject);
    }
    private void doJob(String content, String subject){
        List<String> emails = RegisterService.readEmails();
        for (String email: emails
        ) {
            mailService.sendMail(email,subject,content);
        }
    }
}
