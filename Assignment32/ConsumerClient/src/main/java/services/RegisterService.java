package services;

import java.util.List;

public class RegisterService {
    private static String FOLDER_NAME = "RegistrationStore.txt";
    public static void createReg(){
        FileService.create(".\\",FOLDER_NAME);

    }

    public static void addEmail(String email){
        FileService.write(".\\",FOLDER_NAME, email);
    }
    public static List<String> readEmails(){
        return FileService.readFile(".\\",FOLDER_NAME);
    }
}
