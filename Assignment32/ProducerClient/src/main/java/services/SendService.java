package services;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class SendService {
    private final static String QUEUE_NAME = "DVD";
    private static ConnectionFactory factory = new ConnectionFactory();
    public static void configure(String host){

        factory.setHost(host);

    }
    public static boolean sendMsg(String msg){
    Connection connection = null;
        try {
        connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        channel.basicPublish("", QUEUE_NAME, null, msg.getBytes("UTF-8"));
        System.out.println(" [x] Sent '" + msg + "'");
        channel.close();
        connection.close();
        return true;
    } catch (
    TimeoutException | IOException e) {
        e.printStackTrace();
    }
        return false;
    }
}
