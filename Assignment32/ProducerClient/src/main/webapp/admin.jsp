<%@ page import="java.util.List" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
<head>
    <meta charset="UTF-8">
    <title> </title>
    <link rel="stylesheet" href="styles/adminStyle.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/admin.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
</head>
<body>

    <div class="modal-n">
      <div class="hide-modal"><i class="fas fa-times"></i>

      </div>
      <div class="modal-content">
        <input id="inName" style="text-align: center;" class="inpt-modal" type="text" name="" value="" placeholder="Name">
        <div class="act-btn">
          Submit
        </div>
      </div>
    </div>
    <div class="modal-i">
      <div class="hide-modal"><i class="fas fa-times"></i>

      </div>
      <div class="modal-content">
        <textarea id="inInfo" class="inpt-modal" name="info" rows="12" cols="25" placeholder="Infos"></textarea>
        <div class="act-btn">
          Submit
        </div>
      </div>
    </div>
    <div class="table-box">
      <div class="main-table">
        <div class="tb-wrp">
          <table  border="1">
              <tr class="head-row">
                <th class="head-d">Name</th>
                <th class="head-d">Actions</th>
              <tr>
              <%
                  List<String> strings = (List<String>) request.getAttribute("dvds");
                  if(strings.size() > 0){
                      for (String s: strings) {

                  %>
              <tr class="body-row" >
                <td class="body-d" ><%=s%></td>
                <td class="body-d" >
                  <div class="up-btn">Add Info</div>
                </td>
              </tr>
              <%
                      }
                  }
              %>
          </table>
        </div>
        <div id="insert-btn" class="in-btn">Insert</div>
    </div>
    </div>
</body>
</html>
