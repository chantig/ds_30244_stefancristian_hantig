<html>
<head>
    <meta charset="UTF-8">
    <title> </title>
    <link rel="stylesheet" href="styles/indexStyle.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
</head>
<body>
    <div class="login-box">
        <form class="login-form" method="post" action="/login">
            <h1> Welcome</h1>
            <input class="txtb" type="text" name="username" placeholder="Username">
            <input class="txtb" type="password" name="password" placeholder="Password">
            <input class="login-btn" type="submit" name="" value="Login">
        </form>
        <a class="to" href="register.jsp"><i class="fas fa-sign-out-alt"></i>
          To Register
        </a>
    </div>
</body>
</html>
