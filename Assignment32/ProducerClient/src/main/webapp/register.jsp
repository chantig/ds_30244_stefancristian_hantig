<html>
<head>
    <meta charset="UTF-8">
    <title> </title>
    <link rel="stylesheet" href="styles/registerStyle.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
</head>
<body>
    <div class="login-box">
        <form class="login-form" method="post" action="/register">
            <h1> Stay Notified</h1>
            <input class="txtb" type="email" name="email" placeholder="Email">
            <input class="login-btn" type="submit" name="" value="Register">
        </form>
        <a class="to" href="index.jsp"><i class="fas fa-sign-out-alt"></i>
          To Login
        </a>
    </div>
</body>
</html>
