#Assignmen32
##Introduction
Proiectul consta dintr-o platforma de dvd storage. Un client se inregistreaza, si primeste notificari 
cu noile dvd-uri introduse.
##Build and execution
Proiectul consta dintr-un web-server, consumer si queue-server. Pentru fiecare parte exista un modul diferit. Acestea sunt controlate
folosind maven API. Pentru executia acestora, intai pornim queue-server(un service de tipul rabbit-mq), dupa consumerul, si in cele din urma webserverul.

##Conceptual Architecture Diagram
![Architecture diagram](images/arch.png)
##Deployment Diagram
![Deployment Diagram](images/deploy.png)