package services;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileService {
    public static void create(String dir,String name){
        File file = new File(dir,name);
        System.out.println();
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void write(String dir, String name, String info){
        File file = new File(dir,name);
        FileWriter writer = null;
        try {
            writer = new FileWriter(file,true);
            writer.write(info + "\r\n");
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        }
    public static List<String> getCrede(String name){
        List<String> dvds = new ArrayList<>();
        File path = new File(name);

        File [] files = path.listFiles();
        if (files != null){
            for (File file : files) {
                if (file.isFile()) { //this line weeds out other directories/folders
                    System.out.println(file);
                    dvds.add(file.getName().substring(0, file.getName().length() - 4));
                }
            }
        }
        return dvds;
    }
    public static List<String> readFile(String dir,String filename)
    {
        List<String> content = new ArrayList<>();
        File file = new File(dir,filename); // For example, foo.txt
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                content.add(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }

}
