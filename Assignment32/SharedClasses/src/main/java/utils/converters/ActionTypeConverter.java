package utils.converters;

import utils.types.ActionType;

public class ActionTypeConverter {
    public static String convertTo(ActionType actionType) {
        switch (actionType) {
            case INSERT:
                return "INSERT";
            case UPDATE:
                return "UPDATE";
            case REGISTER:
                return "REGISTER";
            default:
                return "NONE";

        }
    }

    public static ActionType convertFrom(String s) {
        switch (s) {
            case "INSERT":
                return ActionType.INSERT;
            case "REGISTER":
                return ActionType.REGISTER;
            case "UPDATE":
                return ActionType.UPDATE;
            default:
                return ActionType.NONE;
        }
    }
}
