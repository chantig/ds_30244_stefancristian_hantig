﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Storigi.Persistence.Models
{
    public class Content
    {

        public int Id { set; get; }
        public string Name { set; get; }
        public int TopicId { set; get; }
        public int Ups { set; get; }
        public int Downs { set; get; }
    }
}
