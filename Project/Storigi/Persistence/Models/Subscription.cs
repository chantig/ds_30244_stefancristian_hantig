﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Storigi.Persistence.Models
{
    public class Subscription
    {
        public int Id { set; get; }
        public int TopicId { set; get; }
        public Topic Topic { set; get; }

        public int UserId { set; get; }
        public UserStat User { set; get; }
    }
}
