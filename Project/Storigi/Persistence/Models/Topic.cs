﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Storigi.Persistence.Models
{
    public class Topic
    {
        public int Id { set; get; }
        public string Name { set; get; }
        public DateTime UpDateTime { set; get; }

        public IList<Subscription> TopicSubscriptions { set; get; }
    }
}
