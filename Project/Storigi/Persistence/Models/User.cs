﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Storigi.Utils.Types;

namespace Storigi.Persistence.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; } 
        public string Username { set; get; }
        public string Password { set; get; }
        public string Email { set; get; }
        public UserType.UserTypeEnum Type { get; set; }
    }
}
