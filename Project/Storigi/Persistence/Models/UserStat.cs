﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Storigi.Persistence.Models
{
    public class UserStat
    {
        public int Id { set; get; }
        public int UserId { set; get; }
        public int NoOfDowns { set; get; }
        public int NoOfUps { set; get; }

        public IList<Subscription> UserSubscriptions { set; get; }
    }
}
