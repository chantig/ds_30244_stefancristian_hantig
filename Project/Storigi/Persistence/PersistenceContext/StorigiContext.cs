﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Storigi.Persistence.Models;

namespace Storigi.Persistence.PersistenceContext
{
    public class StorigiContext : DbContext
    {
        public StorigiContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<User> Users { set; get; }
        public DbSet<Content> Contents { set; get; }
        public DbSet<Subscription> Subscriptions { set; get; }
        public DbSet<Topic> Topics { set; get; }
        public DbSet<UserStat> UserStats { set; get; }
    }
}
