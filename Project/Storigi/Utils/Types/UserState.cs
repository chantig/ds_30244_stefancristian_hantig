﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Linq;
using System.Threading.Tasks;

namespace Storigi.Utils.Types
{
    public class UserState
    {
        public enum UserStateEnum
        {
            BAN, NOTBAN, NONE
        }

        public static string ToString(UserStateEnum stateEnum)
        {
            switch (stateEnum)
            {
                case UserStateEnum.BAN:
                    return "BAN";
                case UserStateEnum.NOTBAN:
                    return "NOTBAN";
            }
            return "NONE";
        }

        public static UserStateEnum FromString(string userState)
        {
            switch (userState)
            {
                case "BAN":
                    return UserStateEnum.BAN;
                case "NOTBAN":
                    return UserStateEnum.NOTBAN;
            }
            return UserStateEnum.NONE;
        }
    }
}
