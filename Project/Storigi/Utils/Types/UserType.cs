﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Storigi.Utils.Types
{
    public class UserType
    {
        public enum  UserTypeEnum
        {
            REGULAR, ADMIN, NONE
        }

        public static string ToString(UserTypeEnum userType)
        {
            switch (userType)
            {
                case UserTypeEnum.ADMIN:
                    return "ADMIN";
                case UserTypeEnum.REGULAR:
                    return "REGULAR";
            }

            return "NONE";
        }

        public static UserTypeEnum FromString(string userType)
        {
            switch (userType)
            {
                case "REGULAR":
                    return UserTypeEnum.REGULAR;
                case "ADMIN":
                    return UserTypeEnum.ADMIN;
            }

            return UserTypeEnum.NONE;
        }

        public static int ToInt(UserTypeEnum typeEnum)
        {
            switch (typeEnum)
            {
                case UserTypeEnum.ADMIN:
                    return 1;
                case UserTypeEnum.REGULAR:
                    return 0;
            }
            return 2;
        }
    }
}
